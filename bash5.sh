#!/usr/bin/env bash
#SBATCH --job-name=pomo20gcn
#SBATCH --output=pomo20gcn%j.log
#SBATCH --error=pomo20gcn%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash

DEVICES="0"

CUDA_VISIBLE_DEVICES="$DEVICES" python pomo20_gcn.py



