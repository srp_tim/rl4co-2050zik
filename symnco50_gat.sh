#!/usr/bin/env bash
#SBATCH --job-name=symnco20-50gat
#SBATCH --output=symnco20-50gat%j.log
#SBATCH --error=symnco20-50gat%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash


DEVICES="0"

wandb login 9815f3b0e8ce08b8878d8ca9a84bca3dd70f3978
wandb offline

CUDA_VISIBLE_DEVICES="$DEVICES" python symnco50_gat.py



