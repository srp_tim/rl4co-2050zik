#!/usr/bin/env bash
#SBATCH --job-name=symnco50#SBATCH --output=symnco50%j.log
#SBATCH --error=symnco50%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash

DEVICES="0"

CUDA_VISIBLE_DEVICES="$DEVICES" python symnco50.py



