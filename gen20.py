%load_ext autoreload
%autoreload 2

import sys; sys.path.append(3*'../')

import os
import glob
from pathlib import Path
from omegaconf import DictConfig
import yaml
import pickle
from collections import defaultdict


import torch
import lightning as L

from rl4co.envs import TSPEnv, CVRPEnv
from rl4co.tasks.rl4co import RL4COLitModule
from rl4co.utils.lightning import load_model_from_checkpoint, clean_hydra_config
from rl4co.tasks.eval import evaluate_policy

device = torch.device("cuda:0")

exp_name = 'tsp20'
env = TSPEnv()

solver_path = Path("../solver/tsp")
solver_files = glob.glob(str(solver_path / "*tsp*.pkl"), recursive=True)


# Concorde
def load_solver_file(num_nodes):
    # load pickle of first solver file
    file_ = solver_path / f"tsp_{num_nodes}.pkl"
    with open(file_, 'rb') as f:
        costs = pickle.load(f)
    print("Concorde time for {} nodes: {:.2f}s".format(num_nodes, costs[0]))
    print("Average cost: {:.2f}".format(sum(costs[1])/len(costs[1])))
    return costs[1]

# Get all files that contain "generalization*.npz" under data/tsp
data_dir = Path("../data/tsp")
files = glob.glob(str(data_dir / "*generalization*.npz"), recursive=True)
# files =  sorted(files)
# print(files)

num_nodes = [10, 20, 50, 75, 100, 125, 150, 200, 500, 1000]


datasets = []
for num_nodes_ in num_nodes:
    for file in files:
        if "tsp{}_".format(num_nodes_) in file:
            dataset = env.dataset(filename=file, phase='test')
            solver_costs = load_solver_file(num_nodes_)
            datasets.append({'num_nodes': num_nodes_, 'file': file,
                             'dataset': dataset, 'solver_costs': solver_costs})


def load_policy(exp_name, epoch, model_name):

    main_dir = Path('../../../saved_checkpoints/')

    main_dir = main_dir / exp_name / model_name

    cfg_path = main_dir / 'config.yaml'
    ckpt_path = main_dir / 'epoch_{}.ckpt'.format(epoch)

    lit_module = load_model_from_checkpoint(cfg_path, ckpt_path, phase='test')

    policy = lit_module.model.policy.to(device)
    policy.eval()
    return policy

def optimality_gap(bk_sol, opt_sol):
    return ((bk_sol - opt_sol) / bk_sol).mean().item() * 100

def get_generalization_stats(policy):
    stats = defaultdict(list)

    for dataset in datasets:
        print("====================")
        print("Evaluating dataset: {}".format(dataset['file']))
        stats['num_nodes'].append(dataset['num_nodes'])
        stats['file'].append(dataset['file'])
        out_dict = evaluate_policy(env, policy, dataset['dataset'], method='greedy', max_batch_size=128)

        model_costs = -out_dict['rewards']
        solver_costs = torch.Tensor(dataset['solver_costs'][:1000]) # by default, we only evaluate on 1000 samples

        stats['model_cost_mean'].append(model_costs.mean())
        stats['solver_cost_mean'].append(solver_costs.mean())
        gap = optimality_gap(model_costs, solver_costs)
        stats['optimality_gap'].append(gap)

        print("Optimality gap: {:.2f}%".format(gap))

    return stats

# AM Critic
model_name = f"am-critic-{exp_name}"
epoch = "099"
model = load_policy(exp_name, epoch, model_name)
am_critic_stats = get_generalization_stats(model)

# POMO
model_name = f"pomo-{exp_name}"
epoch = "099"
model = load_policy(exp_name, epoch, model_name)
pomo_stats = get_generalization_stats(model)

# SymNCO
model_name = f"symnco-{exp_name}"
epoch = "099"
model = load_policy(exp_name, epoch, model_name)
symnco_stats = get_generalization_stats(model)

experiments = {
    'am-critic': am_critic_stats,
    'am': am_stats,
    'pomo': pomo_stats,
    'symnco': symnco_stats,
    'am-xl': am_xl_stats,
}

colors = {
    'am-critic': 'tab:red',
    'am': 'tab:blue',
    'pomo': 'tab:orange',
    'symnco': 'tab:green',
    'am-xl': 'tab:purple',
}

labels = {
    'am-critic': 'AM-critic',
    'am': 'AM',
    'pomo': 'POMO',
    'symnco': 'SymNCO',
    'am-xl': 'AM-XL',
}
