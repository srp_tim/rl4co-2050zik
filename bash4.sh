#!/usr/bin/env bash
#SBATCH --job-name=pomo50#SBATCH --output=pomo50%j.log
#SBATCH --error=pomo50%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash

DEVICES="0"

CUDA_VISIBLE_DEVICES="$DEVICES" python pomo50.py



